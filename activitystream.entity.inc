<?php

/**
 * @file
 * Wrapprer functions for Activity Stream module.
 */

/**
 * Create a Activity Stream object.
 */
function activitystream_create($values = array()) {

  return entity_get_controller('activitystream_item')->create($values);
}

/**
 * Saves Activity Stream to database.
 */
function activitystream_save(ActivityStream $activitystream) {

  return $activitystream->save();
}

/**
 * Menu autoloader for Activity Stream. Fetch a activitystream object.
 *
 * @param $activitystream_id
 *   Integer specifying the activitystream id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   A fully-loaded activitystream object or FALSE if it cannot be loaded.
 *
 * @see activitystream_load_multiple()
 */
function activitystream_load($activitystream_id, $reset = FALSE) {

  $activitystream_items = activitystream_load_multiple(array($activitystream_id), array(), $reset);

  return reset($activitystream_items);
}

/**
 * Menu autoloader for a particular Activity Stream Service. Fetch a activitystream_item type object.
 *
 * @param $activitystream_id
 *   Integer specifying the activitystream id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   A fully-loaded activitystream object or FALSE if it cannot be loaded.
 *
 * @see activitystream_load_multiple()
 */
function activitystream_service_load($activitystream_service, $reset = FALSE) {

  $activitystream_items = activitystream_load_multiple(array($activitystream_id), array(), $reset);

  return reset($activitystream_items);
}


/**
 * Load multiple Activity Stream based on certain conditions.
 *
 * @param $activitystream_ids
 *   An array of activitystream IDs.
 * @param $conditions
 *   An array of conditions to match against the {activitystream_item} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of activitystream objects, indexed by id.
 *
 * @see entity_load()
 * @see activitystream_load()
 */
function activitystream_load_multiple($activitystream_ids = array(), $conditions = array(), $reset = FALSE) {

  return entity_load('activitystream_item', $activitystream_ids, $conditions, $reset);
}

/**
 * Deletes a Activity Stream.
 */
function activitystream_delete(ActivityStream $activitystream) {

  $activitystream->delete();
}

/**
 * Delete multiple Activity Stream.
  *
 * @param $activitystream_ids
 *   An array of $activitystream IDs.
 */
function activitystream_delete_multiple(array $activitystream_ids) {

  entity_get_controller('activitystream_item')->delete($activitystream_ids);
}
