<?php

/**
 * @file
 * Page callbacks for the Activity Stream module.
 */

/**
 * Menu callback; show the default activity stream page.
 */
function activitystream_default_page($account = NULL, $count = 20) {

  $uid = is_object($account) ? $account->uid : NULL;

  if (!empty($uid)) { // We need a purdier title for user pages.
    drupal_set_title(t("@name's Activity Stream", array('@name' => format_username($account))));
  } // "Pikachu's Activity Stream" sounds more "active" than "Activity Stream for Pikachu."

  $services = activitystream_services_load();
  $activitystream_items = activitystream_items_load($uid, $count);

  $build['activitystream_items']['#theme_wrappers'][]  = 'activitystream_items_wrapper';
  $build['activitystream_items']['#attached']['css'][] = drupal_get_path('module', 'activitystream') . '/activitystream.css';

  foreach ($activitystream_items as $activitystream_item) {
    $service = $activitystream_item->service;
    $build['activitystream_items'][$activitystream_item->id] = array(
      '#activitystream_item' => $activitystream_item,
      '#service'             => $services[$service],
      '#theme'               => 'activitystream_item__' . $service,
    );
  }

  if (count($activitystream_items) == 0) {
    $build['activitystream_sadness'] = array(
      '#markup' => t('No activity items have been saved for this stream yet.'),
    );
  }

  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Block callback; show the default activity stream block.
 */
function activitystream_default_block($account = NULL, $count = 10, $type = 'wide') {
  // Piggy-back off the default page, but without pagers.
  $build = activitystream_default_page($account, $count);
  unset($build['pager']); // Breaks with multiple pages.

  $build['activitystream']['see-more'] = array(
    '#href'   => is_object($account) ? 'user/' . $account->uid . '/activity-stream' : 'activity-stream',
    '#prefix' => '<div class="activitystream-see-more">',
    '#title'  => t('View full activity stream »'),
    '#suffix' => '</div>',
    '#type'   => 'link',
  );

  return $build;
}

/**
 * Default theme for the wrapper around all Activity Stream items.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render containing the user's achievements page.
 */
function theme_activitystream_items_wrapper($variables) {

  return '<div id="activitystream-items">' . $variables['element']['#children'] . '</div>';
}

/**
 * Callback for /activitystream/<ID> page.
 *
 * Just a place to render a complete Activity Stream entity.
 *
 * @param $activitystream
 *   The Activity Stream object to be rendered.
 */
function activitystream_view($activitystream) {

  drupal_set_title(entity_label('activitystream_item', $activitystream));
  $id = entity_id('activitystream', $activitystream);

  return entity_view('activitystream_item', array($id => $activitystream));
}
