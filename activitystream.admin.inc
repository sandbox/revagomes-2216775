<?php

/**
 * @file
 * Activity Stream editing UI.
 */

/**
 * Form callback: create or edit a activitistream.
 *
 * @param $activitystream
 *   The activitystream object to edit or an empty activitystream object
 *   for a create form with only a activitystream type defined.
 */
// @todo Finish disabling the fields.
function activitystream_item_form($form, &$form_state, $activitystream) {

  // Adds the activitystream property field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Activity Stream Name'),
    '#description' => t('The title of this Activity Stream item.'),
    '#default_value' => isset($activitystream->title) ? $activitystream->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $services = activitystream_services_load();

  foreach ($services as $service => $info) {
    $options[$service] = $info['name'];
  }

  $form['service'] = array(
    '#type' => 'select',
    '#title' => t('Service'),
    '#options' => $options,
    '#description' => t('The Activity Stream service where this item was imported.'),
    '#default_value' => isset($activitystream->service) ? $activitystream->service : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#disabled' => isset($activitystream->service) ? TRUE : FALSE,
    '#weight' => -4,
  );

  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User id'),
    '#description' => t('The id of the user that owns this item.'),
    '#default_value' => isset($activitystream->uid) ? $activitystream->uid : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#disabled' => TRUE,
    '#weight' => -4,
  );

  // Add the field related form elements.
  // $form_state['activitystream'] = $activitystream;

  field_attach_form('activitystream_item', $activitystream, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // Adds the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save activitystream'),
    '#submit' => $submit + array('activitystream_item_form_submit'),
  );

  if (!empty($activitystream->title)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete activitystream'),
      '#suffix' => l(t('Cancel'), 'admin/content/activitystream'),
      '#submit' => $submit + array('activitystream_item_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // Appends the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'activitystream_item_form_validate';

  return $form;
}

/**
 * Form API validate callback for the Activity Stream form.
 */
function activitystream_item_form_validate(&$form, &$form_state) {

  $activitystream = $form_state['activitystream_item'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('activitistream_item', $activitystream, $form, $form_state);
}


/**
 * Form API submit callback for the Activity Stream form.
 *
 * @todo remove hard-coded link
 */
function activitystream_item_form_submit(&$form, &$form_state) {

  $activitystream = entity_ui_form_submit_build_entity($form, $form_state);

  drupal_set_message(t('@activitystream Activity Stream item has been saved.', array('@activitystream' => $activitystream->title)));

  $activitystream->save();

  $form_state['redirect'] = 'activitystream/' . $activitystream->id;
}

/**
 * Submit function for for Activity Stream delete action.
 */
function activitystream_item_form_submit_delete($form, &$form_state) {

  $form_state['redirect'] = ACTIVITYSTREAM_ADMIN_CONTENT_MANAGE_URI . '/' . $form_state['activitystream_item']->id . '/delete';
}

/**
 * Form callback wrapper: create or edit a Activity Stream.
 *
 * @param $activitystream
 *   The activitystream object being edited by this form.
 *
 * @see activitystream_item_form()
 */
function activitystream_edir_wrapper($activitystream) {

  $id = $activitystream->id;

  return drupal_goto(ACTIVITYSTREAM_ADMIN_CONTENT_URI . '/manage/' . $id);
}


/**
 * Page to add Activity Stream Entities.
 *
 * @todo Pass this through a proper theme function
 */
function activitystream_add_page() {

  $controller = entity_ui_controller('activitistream_item');

  return $controller->addTypePage();
}
