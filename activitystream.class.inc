<?php

/**
 * @file
 * Classes for Activity Stream module.
 */

module_load_include('inc', 'activitystream', 'activitystream.admin');

/**
 * Activity Stream entity class.
 */
class ActivityStream extends Entity {

  public function __construct($values = array()) {

    parent::__construct($values, 'activitystream_item');
  }

  protected function defaultLabel() {

    return $this->title;
  }

  protected function defaultUri() {

    return array('path' => 'activitystream/' . $this->identifier());
  }
}

/**
 * Custom controller for the Activity Stream entity.
 */
class ActivityStreamController extends EntityAPIController {

  public function __construct($entityType) {

    parent::__construct($entityType);
  }

  /**
   * Overrides the create method.
   */
  public function create(array $values = array()) {

    // Add values that are specific to our entity
    $values += array(
      'id' => '',
      'title' => '',
      'service' => '',
      'is_new' => TRUE,
      'created' => '',
      'changed' => '',
      'data' => '',
      'uid' => '',
      'language' => 'und',
      'status' => 1, // Publish items by default,
    );

    $activitystream = parent::create($values);

    return $activitystream;
  }

  /**
   * Overrides the save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    if (isset($entity->is_new)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->changed = REQUEST_TIME;

    return parent::save($entity, $transaction);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    // Make service field themed like default fields.
    $content['service'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Service'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_service',
      '#field_type' => 'text',
      '#entity_type' => 'activitystream_item',
      '#bundle' => $entity->service,
      '#items' => array(array('value' => $entity->service)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $entity->service)
    );

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    return $build;
  }
}

/**
 * UI controller for Activity Stream entity.
 */
class ActivityStreamUIController extends EntityDefaultUIController {

  public function __construct($entity_type, $entity_info) {

    parent::__construct($entity_type, $entity_info);
  }

  /**
   * Overrides the menu hook for default ui controller.
   */
  public function hook_menu() {

    $items = parent::hook_menu();

    $items[$this->path]['title'] = t('Activity Stream');
    $items[$this->path]['description'] = t('Manage Activity Stream, including fields.');
    // $items[$this->path]['access callback'] = 'activitystream_access';
    // $items[$this->path]['access arguments'] = array('administer activitystream');
    $items[$this->path]['type'] = MENU_LOCAL_TASK | MENU_NORMAL_ITEM;

    // @todo Fix the Activity Stream Add page. For now it's unavailable.
    $items[$this->path . '/add']['access callback'] = FALSE;


    return $items;
  }

  /**
   * Create the page that lists all Activity Stream types available.
   */
  public function addTypePage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    // Just show a warning se não houver nenhum tipo de produto.
    elseif ($count === 0) {
      return t("Você ainda não criou nenhum !types!", array('!types' => l(t('Tipo de Produto'), 'admin/structure/produtos')));
    }

    return theme('activitystream_add_list', array('content' => $content));
  }
}
